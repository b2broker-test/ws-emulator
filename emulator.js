var WebSocketServer = require('websocket').server;
var http = require('http');
var connected = false;
var sendedMessages = 0;
const sockerPort = 8888;

var server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});
server.listen(sockerPort, function() {
    console.log((new Date()) + ` Server is listening on port ${sockerPort}`);
});

server.on('error', function(error) {
    console.log(error);
});

wsServer = new WebSocketServer({
    httpServer: server,
    // You should not use autoAcceptConnections for production
    // applications, as it defeats all standard cross-origin protection
    // facilities built into the protocol and the browser.  You should
    // *always* verify the connection's origin and decide whether or not
    // to accept it.
    autoAcceptConnections: false
});

function originIsAllowed(origin) {
  // put logic here to detect whether the specified origin is allowed.
  return true;
}

wsServer.on('request', function(request) {
    if (!originIsAllowed(request.origin)) {
      // Make sure we only accept requests from an allowed origin
      request.reject();
      console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
      return;
    }
    
    var connection = request.accept('echo-protocol', request.origin);
    console.log((new Date()) + ' Connection accepted.');
    connected = true;
    sendedMessages = 0;
    while(connected) {
        console.log('connection send');
        connection.send(JSON.stringify({id: sendedMessages.toString(), color: 'red', int: 1, float: 1.2, child: { id: (sendedMessages + 1).toString(), color: 'green' }}));
        sendedMessages++;
        if (sendedMessages > 10000) {
            connected = false;
            console.log(sendedMessages);
            console.log('connection false');
        }
    }
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log('Received Message: ' + message.utf8Data);
            connection.sendUTF(message.utf8Data);
        }
        else if (message.type === 'binary') {
            console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
            connection.sendBytes(message.binaryData);
        }
    });
    connection.on('close', function(reasonCode, description) {
        connected = false;
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});

wsServer.on('error', function (error) {
    console.log(error);
});